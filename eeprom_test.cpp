/*
 Update 2014 - TMRh20
 */

/**
 * Simplest possible example of using RF24Network,
 *
 * RECEIVER NODE
 * Listens for messages from the transmitter and prints them out.
 */



#include <cstdlib>
#include <iostream>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <ctime>
#include <stdio.h>
#include <time.h>

/**
 * g++ -L/usr/lib main.cc -I/usr/include -o main -lrrd
 **/
using namespace std;

// CE Pin, CSN Pin, SPI Speed

// Setup for GPIO 22 CE and GPIO 25 CSN with SPI Speed @ 1Mhz
//RF24 radio(RPI_V2_GPIO_P1_22, RPI_V2_GPIO_P1_18, BCM2835_SPI_SPEED_1MHZ);

// Setup for GPIO 22 CE and CE0 CSN with SPI Speed @ 4Mhz
//RF24 radio(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ); 

// Setup for GPIO 22 CE and CE1 CSN with SPI Speed @ 8Mhz
RF24 radio(RPI_V2_GPIO_P1_18, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_8MHZ);  

RF24Network network(radio);

// Address of our node in Octal format (01,021, etc)
const uint16_t this_node = 00;

// Address of the other node
const uint16_t other_node = 02;
int add = 02;
const unsigned long interval = 2000; //ms  // How often to send 'hello world to the other unit

unsigned long last_sent;             // When did we last send?
unsigned long packets_sent;          // How many have we sent already


struct payload_t {                  // Structure of our payload
  uint16_t node_address ;
  uint8_t leaf;
  uint8_t valid_flag; // Valid = 0xdd
  uint16_t unique_id;
  uint16_t sleep_time;
};

int main(int argc, char** argv) 
{
	// Refer to RF24.h or nRF24L01 DS for settings

	radio.begin();

	
	delay(5);
	network.begin(/*channel*/ 90, /*node address*/ this_node);
	radio.printDetails();
	
	while(1){

		network.update();

//		std::cout << "This is a test program and wil only talk to node 01" << std::endl;
//		std::cout << "Please enter node address: "<< std::endl;
//		int node_address_t;
//		std::cin >> node_address_t;
//		std::cout <<  "Please enter if leaf node True/False " << std::endl;
//		bool leaf_t;
//		std::cin >>  leaf_t;
//		std::cout << "Please enter uniquie id for node" << std::endl;
//		int unique_id_t;
//		std::cin >> unique_id_t;
//		std::cout << "Check the details arte correct" << std::endl;
//		std::cout << "Node Address =" << node_address_t << std::endl;
//		std::cout << "Leaf =" << leaf_t << std::endl;
//		std::cout << "Unique id =" << unique_id_t << std::endl;
//		int valid_flag_t = 0xff;
//		char test;
//		std::cout << "Is this correct Y/N:" << std::endl;
//		std::cin >> test;
//		if (test == 'y'){
//			valid_flag_t = 0xdd;
//		}
//		if (valid_flag_t == 0xdd){
//			network.update();
//			payload_t payload = { node_address_t, leaf_t, valid_flag_t, unique_id_t  };
//			RF24NetworkHeader header(/*to node*/ other_node);
//			bool ok = network.write(header,&payload,sizeof(payload));
//			if (ok){
//		  		printf("ok.\n");
//			}else{
//      				printf("failed.\n");
//  			}
//		}

		payload_t payload = { add, 0, 0xdd, 1, 2  };
		RF24NetworkHeader header(/*to node*/ other_node);
		bool ok = network.write(header,&payload,sizeof(payload));
		if (ok){
	  		printf("ok.\n");
		}else{
      			printf("failed.\n");
		}
		break;
	}

	return 0;

}

