
//Includes
#include <cstdlib>
#include <iostream>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <ctime>
#include <stdio.h>
#include <time.h>
#include <stack>
#include <sqlite3.h> 
#include <sstream>
#include <string>

//defines

#define VALID_FLAG 0xdd
#define MASTER_NODE 00
#define RADIO_CHANNEL 90
#define MAX_ADDRESS 5555 // highest address allowed by RF24 library 05555
#define DBFILE "/home/pi/.TempServerDatabase/SensorData.db" // location of database file
#define TEMPTABLE "tempreture" // name of table iused for tempreture
#define TEMPCOLUMNS "ID, temp, humidity, lightlev, chargecurrent" // Tempreture table columns in order
#define SENSORTABLE "sensors"

// Setup for GPIO 18 CE and CE0 CSN with SPI Speed @ 8Mhz
RF24 radio(RPI_V2_GPIO_P1_18, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_8MHZ);  // Radio and network object
RF24Network network(radio);

sqlite3 *db; // database object
char *zErrMsg = 0;

struct sensordata_t {                  // Structure of our received data
	unsigned long lightlev;
	unsigned long temperature;
	unsigned long humidity;
	unsigned long chargecurrent;
	uint16_t leaf_node;
	uint16_t unique_id;
	uint16_t sleep_time;
	uint16_t node_address;
	uint16_t packet_id;
	unsigned char packet_type;
};

struct eeprom_t {                  // Structure of our sent config payload
	uint16_t node_address ;
	uint8_t leaf;
	uint8_t valid_flag; // Valid = 0xdd
	uint16_t unique_id;
	uint16_t sleep_time;
};
//################################SENSORS###################################
class Sensornet_t{
// class to deal with sensors	

	private:
		
		struct sensorpacket_t {                  // Structure of our received payload
			unsigned long lightlev;
			unsigned long temperature;
			unsigned long humidity;
			unsigned long chargecurrent;
			uint16_t leaf_node;
			uint16_t unique_id;
			uint16_t sleep_time;
		};	
		
		uint16_t other_node_address;
		uint16_t node_address ; 
		uint8_t valid_flag; // Valid = 0xdd
		uint16_t leaf_node;
		uint16_t unique_id;
		uint16_t sleep_time;
		
		bool isAddressValid(uint16_t address){
			//Cgeck is address given is within valid ranges
			//1 - 5555 only using digits 1 - 5
			if (address > MAX_ADDRESS)
				return false;
						
			std::stack<uint16_t> sd;
			
			while (address > 0){
	    		uint16_t digit = address%10;
			    address /= 10;
    			sd.push(digit);
			}
			while (!sd.empty()){
			    int digit = sd.top();
			    sd.pop();
			    //printf("%i\n", digit);
			    if (digit > 5 || digit == 0)
			    	return false;
			}
			return true;
		}		

		
	public:
	
		eeprom_t create_Payload(uint16_t send_address, uint16_t new_address, uint8_t isleaf, uint16_t uniqueid, uint16_t sleeptime){
			//Create payload from given data and return data in corrct format (eeprom_t)
				
			uint8_t validflag = VALID_FLAG;
					
			if (!isAddressValid(send_address)){ // Check if send_address is valid
				//printf("fail1\n");
				validflag = 0;
			}
				
			if (!isAddressValid(new_address)){ // Check if new_address is valid
				//printf("fail2\n");
				validflag = 0;
			}
			
			other_node_address = send_address; // Set class variables ready for  other functions
			node_address = new_address;
			leaf_node = isleaf;
			sleep_time = sleeptime;
			unique_id = uniqueid; 
							
			eeprom_t payload = {new_address, isleaf, validflag, uniqueid, sleeptime};// Set payload data
				
			printf("ok\n");
			return payload;
				
		} // END create payload END
		
		bool send_Data(eeprom_t &eeprom, int other_node){
			// send data function
			if (eeprom.valid_flag == VALID_FLAG){
		
				RF24NetworkHeader header(/*to node*/ other_node);
				bool ok = network.write(header,&eeprom,sizeof(eeprom));
				if (ok){
	  				//printf("ok.\n");
	  				return true;
				}else{
      				//printf("failed.\n");
      				return false;
				}
			}
		}// END send data END
		
		sensordata_t receive_Data(){
			// Receive data function
			// Receive data from radio and return as struct (sensordata_t)
			network.update();
  		  	while ( network.available() ) {     // Is there anything ready for us?
  		  		sensorpacket_t payload;
  		  		sensordata_t payloaddata;
    			RF24NetworkHeader header;        // If so, grab it and print it out
  				network.read(header,&payload,sizeof(payload));
  				//printf("Payload #%i frm %i id=%i. led %lu tmp %luC hum %lu batt %lu. Sleep=%i. Leaf%i. \n",header.id, header.from_node, payload.unique_id, payload.lightlev, payload.temperature, payload.humidity, payload.chargecurrent, payload.sleep_time, payload.leaf_node);
  				payloaddata.lightlev = payload.lightlev; // add data to return struct.
  				payloaddata.temperature = payload.temperature;
  				payloaddata.humidity = payload.humidity;
  				payloaddata.chargecurrent = payload.chargecurrent;
  				payloaddata.unique_id = payload.unique_id;
  				payloaddata.leaf_node = payload.leaf_node;
  				payloaddata.sleep_time = payload.sleep_time;
  				payloaddata.node_address = header.from_node;
  				payloaddata.packet_id = header.id;
  				payloaddata.packet_type = header.type;
  				
  				return payloaddata;
  			}
  			sensordata_t payloaddata;
  			return payloaddata;
		} // END Receive data END
};
//###################################END SENSORS CLASS############################################################
//#######################################DATABASE#################################################################

class Database_t{

	private:
	
		char *sql; // sql statement

	
		static int callback(void *NotUsed, int argc, char **argv, char **azColName){
			int i;
			//for(i=0; i<argc; i++){
			//	printf("%s = %s1\n", azColName[i], argv[i] ? argv[i] : "NULL");
			//}
			//	printf("\n");
			printf("%s = %s id0\n", azColName[0], argv[0]);
			printf("%s = %s id1\n", azColName[1], argv[1]);
			printf("%s = %s id2\n", azColName[2], argv[2]);
			printf("%s = %s id3\n", azColName[3], argv[3]);
			printf("%s = %s id4\n", azColName[4], argv[4]);
			printf("%i\n", argc);
			
			return 0;
		}	
	
	public:
		
		//########## Add readings to database.
		bool add_Readings(sensordata_t payload){
		
			std::stringstream strm;
			strm << "INSERT INTO "TEMPTABLE "("TEMPCOLUMNS") VALUES(" << payload.unique_id << ",'" << payload.temperature << "'," << payload.humidity << ",'" << payload.lightlev << "'," << payload.chargecurrent <<")";

    		std::string s = strm.str();
    		sql = &s[0];
		
			//sql = "INSERT INTO tempreture (ID, temp, humidity, lightlev, chargecurrent) "\ // this doesnt work!!!
			//"VALUES ("payload.unique_id", "payload.temperature", "payload.humidity", "payload.lightlev", "payload.chargecurrent");";
			
			int rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   			if( rc != SQLITE_OK ){
				printf("SQL error: %s\n", zErrMsg);
				sqlite3_free(zErrMsg);
				return false;
			}else{
				printf("Records created successfully\n");
				return true;
   			}
		
		}
		
		//######## Get readings from database, needs to be provided with the uniques id of sensor
		sensordata_t get_Readings(uint16_t node_id){
			
			sensordata_t payload;
			
			// this is used to list all (*) the data from node with unique id 4(node_id) and provides the latest(ASC) result and not the oldest(DESC). 
			// select * from (select * from tempreture group by timestamp having MAX(id) = 4 AND MIN(id) = 4 ORDER BY timestamp ASC) group by id;
			
			// Create sql statement
			std::stringstream strm;
			strm << "select * from (select * from tempreture group by timestamp having MAX(id) = " << node_id << " AND MIN(id) = " <<node_id << " ORDER BY timestamp ASC) group by id";

    		std::string s = strm.str();
    		sql = &s[0];
    		
    		int rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   			if( rc != SQLITE_OK ){
				printf("SQL error: %s\n", zErrMsg);
				sqlite3_free(zErrMsg);
			}else{
				printf("Records read successfully\n");
   			}
		}
		
		
};



//####################################END DATABASE CLASS###########################################################

int main(int argc, char** argv){

// ### Main program setup goes here ###

//######## Setup Radio ###########
	radio.begin();
	delay(5);
	network.begin(/*channel*/ RADIO_CHANNEL, /*node address*/ MASTER_NODE);
	radio.printDetails();
	//sensordata_t sensordata;
	Sensornet_t sensornet;
	Database_t database;
	
	
//######### Setup Database ###########

	int rc = sqlite3_open(DBFILE, &db);
	   
   	if( rc ){
    	printf("Can't open database: %s\n", sqlite3_errmsg(db));
    	exit(0);
   	}else{
    	printf("Opened database successfully\n");
   	}
	
	
	while (1){
//### Main program runs here in a never ending loop###

		network.update();
		RF24NetworkHeader header;	
		//sensornet.create_Payload(1111, 1, 0, 1, 0);
		//database.add_Readings(sensornet.receive_Data());
		database.get_Readings(1);
		//sensordata = sensornet.receive_Data();
		//printf("Payload #%i frm %i id=%i. led %lu tmp %luC hum %lu batt %lu. Sleep=%i. Leaf%i. \n",header.id, header.from_node, sensordata.unique_id, sensordata.lightlev, sensordata.temperature, sensordata.humidity, sensordata.chargecurrent, sensordata.sleep_time, sensordata.leaf_node);
		
		delay(1200);
	}
	sqlite3_close(db);
	return 0;
}


