// Includes
#include "Database.h"
#include <cstdlib>
#include <iostream>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <ctime>
#include <stdio.h>
#include <time.h>
#include <stack>
#include <sqlite3.h> 
#include <sstream>
#include <string>

//defines

#define DBFILE "/home/pi/.TempServerDatabase/SensorData.db" // location of database file
#define TEMPTABLE "tempreture" // name of table iused for tempreture
#define TEMPCOLUMNS "ID, temp, humidity, lightlev, chargecurrent" // Tempreture table columns in order
#define SENSORTABLE "sensors"
#define SENSORCOLUMNS "id, address, sleeptime, leafnode, name"

sqlite3 *db; // database object
char *zErrMsg = 0;

Database_t::Database_t()
{
	// Constructer
	//######### Setup Database ###########

	int rc = sqlite3_open(DBFILE, &db);
	   
   	if( rc ){
    	printf("Can't open database: %s\n", sqlite3_errmsg(db));
    	exit(0);
   	}else{
    	printf("Opened database successfully\n");
   	}
}

Database_t::~Database_t()
{
	//Destructer
	printf("closing");
	sqlite3_close(db);
}

// callback used to process data from reading the database, called by sqlite3_exec.
int Database_t::c_callback(void *param, int argc, char **argv, char **azColName)
{
	//int i;
	//for(i=0; i<argc; i++){
	//	printf("%s = %s1\n", azColName[i], argv[i] ? argv[i] : "NULL");
	//}
	//	printf("\n");
	//printf("c-callback\n");
	//printf("%s = %s id0\n", azColName[0], argv[0]);
	//printf("%s = %s id1\n", azColName[1], argv[1]);
	//printf("%s = %s id2\n", azColName[2], argv[2]);
	//printf("%s = %s id3\n", azColName[3], argv[3]);
	//printf("%s = %s id4\n", azColName[4], argv[4]);
	//printf("%s = %s id5\n", azColName[5], argv[5]);
	//printf("\ncallback = %i\n\n", argc);
	sensordata_t* data1 = (sensordata_t*) param;
	//printf("%i\n", argc);
	if( data1->data_type == 0xaa ){
		// Add data from reading tempreture readings
		//sensordata_t* data1 = (sensordata_t*) param;
		data1->unique_id = atoi(argv[0]); // Update payload pointer so get_readings function can access them
		data1->temperature = atoi(argv[1]);
		data1->humidity = atoi(argv[2]);
		data1->lightlev = atoi(argv[3]);
		data1->chargecurrent = atoi(argv[4]);
		data1->timestamp = argv[5];
		// ARGV[6] = key PRIMARY KEY
		//Database_t* data = reinterpret_cast<Database_t*>(param);
		// std::cout << argv[0] << std::endl;
		// return data->callback(argc, argv, azColName);	
			
		return 0;
	}else if( data1->data_type == 0xbb ){
		// add data from reading sensor configs
		//sensordata_t* data1 = (sensordata_t*) param;
		data1->unique_id = atoi(argv[0]);
		data1->node_address = atoi(argv[1]);
		data1->sleep_time = atoi(argv[2]);
		data1->leaf_node = atoi(argv[3]);
		data1->nodename = argv[4];
		data1->timestamp = argv[5];
		//argv[6] = PRIMARY KEY
		
		return 0;
	}else{
	data1->data_type = 0;
		return 1;
	}
}

bool Database_t::add_Readings(sensordata_t payload)
{
	std::stringstream strm;
	strm << "INSERT INTO " << TEMPTABLE << "(" << TEMPCOLUMNS<< ") VALUES(" << payload.unique_id << ",'" << payload.temperature << "'," << payload.humidity << ",'" << payload.lightlev << "'," << payload.chargecurrent <<")";

	std::string s = strm.str();
	sql = &s[0];
	
	//sql = "INSERT INTO tempreture (ID, temp, humidity, lightlev, chargecurrent) "\ // this doesnt work!!!
	//"VALUES ("payload.unique_id", "payload.temperature", "payload.humidity", "payload.lightlev", "payload.chargecurrent");";
			
	int rc = sqlite3_exec(db, sql, c_callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		printf("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return false;
	}else{
		printf("Readings created successfully\n");
		return true;
	}
}

bool Database_t::add_Configs(sensordata_t payload)
{
	// Convert string intpo char array for sqlexec
	std::stringstream strm;
	strm << "INSERT INTO " << SENSORTABLE << "(" << SENSORCOLUMNS<< ") VALUES(" << payload.unique_id << ",'" << payload.node_address << "'," << payload.sleep_time << ",'" << payload.leaf_node << "'," << payload.nodename <<")";

	std::string s = strm.str();
	sql = &s[0];
	
	//sql = "INSERT INTO tempreture (ID, temp, humidity, lightlev, chargecurrent) "\ // this doesnt work!!!
	//"VALUES ("payload.unique_id", "payload.temperature", "payload.humidity", "payload.lightlev", "payload.chargecurrent");";
	printf("%s\n", sql);	
	int rc = sqlite3_exec(db, sql, c_callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		printf("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return false;
	}else{
		printf("Configs created successfully\n");
		return true;
	}

}

sensordata_t Database_t::get_Readings(uint16_t node_id)
{
	sensordata_t payload = {};
			
	// this is used to list all (*) the data from node with unique id 4(node_id) and provides the latest(ASC) result and not the oldest(DESC). 
	//select * from (select * from tempreture group by timestamp having MAX(id) = 4 AND MIN(id) = 4 ORDER BY timestamp ASC) group by id;
	//printf("get readings\n");	
	//Create sql statement
	std::stringstream strm;
	strm << "select * from (select * from tempreture group by timestamp having MAX(id) = " << node_id << " AND MIN(id) = " <<node_id << " ORDER BY timestamp ASC) group by id";

	std::string s = strm.str();
	sql = &s[0];
   	payload.data_type = 0xaa;
	int rc = sqlite3_exec(db, sql, c_callback, &payload, &zErrMsg);
	if( rc != SQLITE_OK ){
		printf("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		payload.data_type = 0;
		return payload;
	}else{
		printf("Records read successfully\n");
		return payload;
		
	}
	
}

sensordata_t Database_t::get_Configs(uint16_t node_id)
{
	// This is used to get stored config data from the database	
	// init all as 0
	sensordata_t payload = {};
	
	// this is used to list all (*) the data from node with unique id 4(node_id) and provides the latest(ASC) result and not the oldest(DESC). 
	//select * from (select * from sensors group by timestamp having MAX(id) = 4 AND MIN(id) = 4 ORDER BY timestamp ASC) group by id;
	//printf("get readings\n");	
	//Create sql statement
	std::stringstream strm;
	strm << "select * from (select * from sensors group by timestamp having MAX(id) = " << node_id << " AND MIN(id) = " <<node_id << " ORDER BY timestamp ASC) group by id";

	std::string s = strm.str();
	sql = &s[0];
   	payload.data_type = 0xbb;
   	
	int rc = sqlite3_exec(db, sql, c_callback, &payload, &zErrMsg);
	if( rc != SQLITE_OK ){
		printf("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		payload.data_type = 0;
		return payload;
	}else{
		printf("Records read successfully\n");
		return payload;
		
	}
}



