#ifndef DATABASE_H
#define DATABASE_H

// includes
#include <stdint.h>
#include <string>
#include "Sensornet.h" //This is needed for the sensordata_t struct


//Defines


//struct sensordata_t {                  // Structure of our received data
//	unsigned long lightlev;			   // Public so can be accessed from antwhere
//	unsigned long temperature;
//	unsigned long humidity;
//	unsigned long chargecurrent;
//	uint8_t leaf_node;
//	uint16_t unique_id;
//	uint8_t sleep_time;
//	uint16_t node_address;
//	uint16_t packet_id;
//	uint8_t data_type; // 0xaa = readings 0xbb = config
//	char* timestamp;
//};

class Database_t
{
	private:
	
		char *sql; // sql statement
		
		uint16_t unique_id;
		uint16_t temperature;
		uint16_t humidity;
		uint16_t lightlev;
		uint16_t chargecurrent;
		char* timestamp;
		
		static int c_callback(void* NotUsed, int argc, char **argv, char **azColName);
		
			
	public:
	
		Database_t(); // constructer
		~Database_t(); //Destructer
		
		//########## Add readings to database.
		bool add_Readings(sensordata_t payload);
		
		//######## Add sensor config data to the database
		bool add_Configs(sensordata_t payload);
		
		//######## Get readings from database, needs to be provided with the uniques id of sensor
		sensordata_t get_Readings(uint16_t node_id);
		
		//######### Get sensor config from the database
		sensordata_t get_Configs(uint16_t node_id);
		
		
};

#endif //DATABASE_H
