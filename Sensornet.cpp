
// Includes
#include "Sensornet.h"
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <stack>


#define MAX_ADDRESS 5555
#define VALID_FLAG 0xdd
#define MASTER_NODE 00
#define RADIO_CHANNEL 90


RF24 radio(RPI_V2_GPIO_P1_18, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_8MHZ);  // Radio and network object
RF24Network network(radio);

// Class constructor
Sensornet_t::Sensornet_t()
{
	

	//######## Setup Radio ###########
	radio.begin();
	delay(5);
	network.begin(/*channel*/ RADIO_CHANNEL, /*node address*/ MASTER_NODE);
	radio.printDetails();
}

bool Sensornet_t::isAddressValid(uint16_t address)
{
	//Cgeck is address given is within valid ranges
	//1 - 5555 only using digits 1 - 5
	if (address > MAX_ADDRESS)
		return false;
				
	std::stack<uint16_t> sd;
		
	while (address > 0){
   		uint16_t digit = address%10;
	    address /= 10;
  			sd.push(digit);
	}
	while (!sd.empty()){
	    int digit = sd.top();
	    sd.pop();
	    //printf("%i\n", digit);
	    if (digit > 5 || digit == 0)
	    	return false;
	}
	return true;
}

eeprom_t Sensornet_t::create_Payload(uint16_t send_address, uint16_t new_address, uint8_t isleaf, uint16_t uniqueid, uint16_t sleeptime)
{
	//Create payload from given data and return data in corrct format (eeprom_t)
		
	uint8_t validflag = VALID_FLAG;
			
	if (!isAddressValid(send_address)){ // Check if send_address is valid
		printf("send address invalid (%i)\n", send_address);
		validflag = 0;
	}
			
	if (!isAddressValid(new_address)){ // Check if new_address is valid
		printf("New address invalid (%i)\n", new_address);
		validflag = 0;
	}
			
	other_node_address = send_address; // Set class variables ready for  other functions
	node_address = new_address;
	leaf_node = isleaf;
	sleep_time = sleeptime;
	unique_id = uniqueid; 
					
	eeprom_t payload = {new_address, isleaf, validflag, uniqueid, sleeptime};// Set payload data
				
	printf("Payload Created.\n");
	return payload;
}

bool Sensornet_t::send_Data(eeprom_t &eeprom, int other_node)
{
	// send data function
	if (eeprom.valid_flag == VALID_FLAG){
	
		RF24NetworkHeader header(/*to node*/ other_node);
		bool ok = network.write(header,&eeprom,sizeof(eeprom));
		if (ok){
			printf("Sent to %i\n", other_node);
			return true;
		}else{
			printf("config send failed\n");
			return false;
		}
	}
//	printf("Data Invalid");
	return false;
}

sensordata_t Sensornet_t::receive_Data()
{
	// Receive data function
	// Receive data from radio and return as struct (sensordata_t)
	network.update();
  	while ( network.available() ) {     // Is there anything ready for us?
  		sensorpacket_t payload;
  		sensordata_t payloaddata;
		RF24NetworkHeader header;        // If so, grab it and print it out
		network.read(header,&payload,sizeof(payload));
		//printf("Payload #%i frm %i id=%i. led %lu tmp %luC hum %lu batt %lu. Sleep=%i. Leaf%i. \n",header.id, header.from_node, payload.unique_id, payload.lightlev, payload.temperature, payload.humidity, payload.chargecurrent, payload.sleep_time, payload.leaf_node);
		payloaddata.lightlev = payload.lightlev; // add data to return struct.
		payloaddata.temperature = payload.temperature;
		payloaddata.humidity = payload.humidity;
		payloaddata.chargecurrent = payload.chargecurrent;
		payloaddata.unique_id = payload.unique_id;
		payloaddata.leaf_node = payload.leaf_node;
		payloaddata.sleep_time = payload.sleep_time;
		payloaddata.node_address = header.from_node;
		payloaddata.packet_id = header.id;
		payloaddata.data_type = 0xaa; // can be header_type for type sent by radio
		//payloaddata.timestamp = "Not Set";
  				
		return payloaddata;
	}
	sensordata_t payloaddata = {};
	return payloaddata;
}
