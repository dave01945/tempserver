#ifndef SENSORNET_H
#define SENSORNET_H
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
//#include <stdint.h>
// Setup for GPIO 18 CE and CE0 CSN with SPI Speed @ 8Mhz
//class RF24;
//class RF24Network;



struct sensordata_t {                  // Structure of our received data
	unsigned long lightlev;			   // Public so can be accessed from antwhere
	unsigned long temperature;
	unsigned long humidity;
	unsigned long chargecurrent;
	uint16_t leaf_node;
	uint16_t unique_id;
	uint16_t sleep_time;
	uint16_t node_address;
	uint16_t packet_id;
	unsigned char data_type; // 0xaa = readings 0xbb = config
	char* timestamp;
	char* nodename;
};

struct eeprom_t{                  // Structure of our sent config payload
	uint16_t node_address ;
	uint16_t leaf;
	uint8_t valid_flag; // Valid = 0xdd
	uint16_t unique_id;
	uint16_t sleep_time;
};

struct sensorpacket_t {                  // Structure of our received payload
	unsigned long lightlev;
	unsigned long temperature;
	unsigned long humidity;
	unsigned long chargecurrent;
	uint16_t leaf_node;
	uint16_t unique_id;
	uint16_t sleep_time;
};


class Sensornet_t
{
	
		
	private:
	
	
		
		uint16_t other_node_address;
		uint16_t node_address ; 
		uint8_t valid_flag; // Valid = 0xdd
		uint16_t leaf_node;
		uint16_t unique_id;
		uint16_t sleep_time;
		


	
		
	
	public:
	
		//struct sensordata_t {                  // Structure of our received data
		//	unsigned long lightlev;			   // Public so can be accessed from antwhere
		//	unsigned long temperature;
		//	unsigned long humidity;
		//	unsigned long chargecurrent;
		//	uint16_t leaf_node;
		//	uint16_t unique_id;
		//	uint16_t sleep_time;
		//	uint16_t node_address;
		//	uint16_t packet_id;
		//	unsigned char packet_type;
			//std::string timestamp;
		//};
		Sensornet_t();// Constrcuter class
		
		bool isAddressValid(uint16_t address);
		eeprom_t create_Payload(uint16_t send_address, uint16_t new_address, uint8_t isleaf, uint16_t uniqueid, uint16_t sleeptime);
		bool send_Data(eeprom_t &eeprom, int other_node);
		sensordata_t receive_Data();

};

#endif // SENSORNET_H
