
//Includes
#include <cstdlib>
#include <iostream>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <ctime>
#include <stdio.h>
#include <time.h>
#include <stack>
#include <sqlite3.h> 
#include <sstream>
#include <string>
#include "Sensornet.h"
#include "Database.h" 

//defines

#define VALID_FLAG 0xdd // also defined in sensornet.cpp
//#define MASTER_NODE 00
//#define RADIO_CHANNEL 90
//#define MAX_ADDRESS 5555 // highest address allowed by RF24 library 05555 This is now defined in sensornet.cpp
//#define DBFILE "/home/pi/.TempServerDatabase/SensorData.db" // location of database file
//#define TEMPTABLE "tempreture" // name of table iused for tempreture
//#define TEMPCOLUMNS "ID, temp, humidity, lightlev, chargecurrent" // Tempreture table columns in order
//#define SENSORTABLE "sensors"


int main(int argc, char** argv){

// ### Main program setup goes here ###


	Sensornet_t sensornet; //Setup radio network
	sensordata_t sensordata; // setup data struct
	Database_t database; // Setup database
	//sensordata_database sensordata_d;
	

	
	
	while (1){
//### Main program runs here in a never ending loop###

		//network.update();
		//RF24NetworkHeader header;
		//eeprom_t eeprom;
		//printf("While1\n");
		//eeprom = sensornet.create_Payload(3, 3, 0, 3, 2); // these 2 lines are how we create and send config data to the sensor	
		//int ok = sensornet.send_Data(eeprom, 3);
		//if (ok)
		//	printf("ok sent\n");
		//else
		//	printf("not ok\n");
		//sensordata = sensornet.receive_Data();
		//sensordata.unique_id = 4;
		//sensordata.node_address = 1;
		//sensordata.sleep_time = 4;
		//sensordata.leaf_node = 0;
		//sensordata.nodename = "'Test'";
		//database.add_Configs(sensordata);
		
		//database.add_Readings(sensornet.receive_Data());
		
		//sensordata = database.get_Configs(4);
		//sensordata = database.get_Readings(4);
		
		//sensordata = sensornet.receive_Data();
		//printf("Payload #%i frm %i id=%i. led %lu tmp %luC hum %lu batt %lu. Sleep=%i. Leaf=%i. type %x \nName: %s\nTime: %s\n",sensordata.packet_id, sensordata.node_address, sensordata.unique_id, sensordata.lightlev, sensordata.temperature, sensordata.humidity, sensordata.chargecurrent, sensordata.sleep_time, sensordata.leaf_node, sensordata.data_type, sensordata.nodename, sensordata.timestamp);
		
		//delay(1200);
// ########################################## END OF TESTING ####################################################################

		sensordata = sensornet.receive_Data();

		if ( sensordata.data_type == 0xaa){
			database.add_Readings(sensordata);
			
			sensordata_t tempdata; // Get reading from database to compare
			tempdata= database.get_Configs(sensordata.unique_id);
			
			if (tempdata.node_address != sensordata.node_address || tempdata.leaf_node != sensordata.leaf_node || tempdata.sleep_time != sensordata.sleep_time){
				// If database has different data then update sensor with new config.
				
				eeprom_t eeprom_data;
				eeprom_data = sensornet.create_Payload(sensordata.node_address, tempdata.node_address, tempdata.leaf_node, sensordata.unique_id, tempdata.sleep_time);
				sensornet.send_Data(eeprom_data, sensordata.node_address);				
			}
		}	
		delay(1);				
	}
	//sqlite3_close(db);
	return 0;
}


